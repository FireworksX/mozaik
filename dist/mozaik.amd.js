define(function () { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    var Units;
    (function (Units) {
        function isPrimitive(value) {
            return !(value instanceof Object);
        }
        Units.isPrimitive = isPrimitive;
        function distructivePath(path) {
            var explodePath = path.split(/[\.\[\]\"\']{1,2}/);
            return explodePath.filter(function (el) {
                return !!el && el.toString().length > 0;
            });
        }
        Units.distructivePath = distructivePath;
        function isObject(target) {
            return target instanceof Object && !Array.isArray(target);
        }
        Units.isObject = isObject;
        function isArray(target) {
            return Array.isArray(target);
        }
        Units.isArray = isArray;
        function isBoolean(target) {
            return typeof target === "boolean";
        }
        Units.isBoolean = isBoolean;
        function isNumber(target) {
            return typeof target === "number";
        }
        Units.isNumber = isNumber;
        function isString(target) {
            return typeof target === "string";
        }
        Units.isString = isString;
        function isFunction(target) {
            return typeof target === "function";
        }
        Units.isFunction = isFunction;
        function isSymbol(target) {
            return typeof target === "symbol";
        }
        Units.isSymbol = isSymbol;
        function makeObjectTree(path, value, state) {
            var _a;
            if (state === void 0) { state = {}; }
            var state$ = state;
            if (path.length === 1) {
                return _a = {}, _a[path[0]] = value, _a;
            }
            else if (path.length > 1) {
                var key = path[0];
                path.shift();
                state$[key] = makeObjectTree(path, value);
                return state$;
            }
        }
        Units.makeObjectTree = makeObjectTree;
    })(Units || (Units = {}));
    //# sourceMappingURL=Units.js.map

    var Debug;
    (function (Debug) {
        function trace(message) {
            console.trace("[Mozaik] " + message);
        }
        Debug.trace = trace;
        function warn(message) {
            console.warn("[Mozaik] " + message);
        }
        Debug.warn = warn;
        function info(message) {
            var options = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                options[_i - 1] = arguments[_i];
            }
            console.info.apply(console, ["[Mozaik] " + message].concat(options));
        }
        Debug.info = info;
        function mutation(ob, newVal, oldVal) {
            this.info("mutation");
        }
        Debug.mutation = mutation;
    })(Debug || (Debug = {}));
    var Debug$1 = Debug;
    //# sourceMappingURL=index.js.map

    var Event = /** @class */ (function () {
        function Event(name, cb) {
            this.name = name;
            this.cb = cb;
            this._id = Event._id;
            Event._id++;
        }
        Event._id = 0;
        return Event;
    }());
    //# sourceMappingURL=Event.js.map

    var EventEmitter = /** @class */ (function () {
        function EventEmitter() {
            this.events = {};
        }
        EventEmitter.prototype.$on = function (event, cb) {
            var event$ = new Event(event, cb);
            if (this.events.hasOwnProperty(event)) {
                this.events[event].push(event$);
            }
            else {
                this.events[event] = [event$];
            }
            return event$;
        };
        EventEmitter.prototype.$emit = function (event, payload) {
            if (this.events.hasOwnProperty(event)) {
                this.events[event].forEach(function (el) {
                    el.cb(payload);
                });
            }
        };
        EventEmitter.prototype.$off = function (event) {
            if (this.events.hasOwnProperty(event.name)) {
                var list = this.events[event.name];
                var index = list
                    .map(function (el) { return el._id; })
                    .indexOf(event._id);
                list.splice(index, 1);
            }
        };
        return EventEmitter;
    }());
    //# sourceMappingURL=EventEmitter.js.map

    var Observer = /** @class */ (function () {
        function Observer(name, target) {
            Observer.id++;
            this.debug = false;
            this._id = Observer.id;
            this.emitter = new EventEmitter();
            this.name = name;
            this.children = [];
            this.target = target;
            this.children = this._defineReactive(this.target);
        }
        Object.defineProperty(Observer.prototype, "value", {
            get: function () {
                return this.target;
            },
            set: function (val) {
                var oldVal = this.target;
                // this.emitter.$emit("beforeChange", {val, oldVal});
                if (this.target !== val) {
                    if (this.debug) {
                        Debug$1.trace("Property [" + this.name + "] has been changed. Value: " + oldVal + " -> " + val + ".");
                    }
                    this.target = val;
                    if (!Units.isPrimitive(val)) {
                        this.appendChildren(val);
                        // this.children = this._defineReactive(val);
                    }
                    // this.emitter.$emit("change", {val, oldVal});
                }
            },
            enumerable: true,
            configurable: true
        });
        Observer.prototype.lock = function (options) {
            return undefined;
        };
        Observer.prototype.unlock = function (options) {
            return undefined;
        };
        Object.defineProperty(Observer.prototype, "childrenMap", {
            get: function () {
                var map = {};
                for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
                    var child = _a[_i];
                    map[child.name] = child;
                }
                return map;
            },
            enumerable: true,
            configurable: true
        });
        Observer.prototype.getChild = function (name) {
            if (this.childrenMap.hasOwnProperty(name)) {
                return this.childrenMap[name];
            }
            else {
                return undefined;
            }
        };
        Observer.prototype.appendChildren = function (target) {
            var newChildrenMap = {};
            if (Units.isObject(target)) {
                for (var key in target) {
                    var child = this.getChild(key);
                    if (!!child) {
                        child.value = target[key];
                    }
                    else {
                        newChildrenMap[key] = target[key];
                    }
                }
            }
            else if (Units.isArray(target)) {
                newChildrenMap = __assign({}, target);
            }
            var childrenTree = this._defineReactive(newChildrenMap);
            // TODO Fix save children when target = object
            this.children = this.children.concat(childrenTree);
            return this.children;
        };
        Observer.prototype.push = function () {
            var _a, _b;
            var items = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                items[_i] = arguments[_i];
            }
            if (Units.isArray(this.value)) {
                var lastArrayIndexFromChildren_1 = this.value.length;
                var reactiveItems = this._defineReactive(items.slice())
                    .map(function (el, index) {
                    el.name = "" + (Number(lastArrayIndexFromChildren_1) + index);
                    return el;
                });
                var oldVal = this.value.slice();
                (_a = this.target).push.apply(_a, items);
                (_b = this.children).push.apply(_b, reactiveItems);
                this.emitter.$emit("change", { val: this.target, oldVal: oldVal });
            }
        };
        Observer.prototype.get = function (path) {
            if (path.length === 1) {
                return this.getChild(path[0]);
            }
            else if (path.length > 1) {
                var child = this.getChild(path[0]);
                if (!!child) {
                    path.shift();
                    return child.get(path);
                }
            }
        };
        Observer.prototype.set = function (path, value, options) {
            var _this = this;
            if (options === void 0) { options = {}; }
            var notify = options.notify;
            if (Units.isArray(path) && path.length > 0) {
                if (path.length === 1) {
                    var getChild = this.getChild(path[0]);
                    if (!!getChild) {
                        var oldVal = getChild.value;
                        getChild.value = value;
                        if (!!notify) {
                            getChild.emitter.$emit("change", { val: value, oldVal: oldVal });
                            getChild.emitter.$emit("update", { val: value, oldVal: oldVal });
                        }
                        else {
                            getChild.emitter.$emit("update", { val: value, oldVal: oldVal });
                        }
                    }
                    else {
                        var ob = new Observer(path[0], value);
                        ob.on("update", function (response) {
                            _this._childrenDidUpdate();
                        });
                        this.children.push(ob);
                        var oldVal = ob.value;
                        if (!!notify) {
                            ob.emitter.$emit("change", { val: value, oldVal: oldVal });
                            ob.emitter.$emit("update", { val: value, oldVal: oldVal });
                        }
                        else {
                            ob.emitter.$emit("update", { val: value, oldVal: oldVal });
                        }
                    }
                }
                else if (path.length > 1) {
                    var nameOb = path[0];
                    path.shift();
                    var findOb = this.getChild(nameOb);
                    if (!!findOb) ;
                    else {
                        var deepTree = Units.makeObjectTree(path, value);
                        findOb = new Observer(nameOb, deepTree);
                        this.children.push(findOb);
                    }
                    findOb.set(path, value, options);
                }
            }
        };
        Observer.prototype.on = function (event, cb) {
            return this.emitter.$on(event, cb);
        };
        Observer.prototype.off = function (event) {
            this.emitter.$off(event);
        };
        Observer.prototype._childrenDidUpdate = function () {
            var decodeTree = {};
            for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
                var ob = _a[_i];
                decodeTree[ob.name] = ob.value;
            }
            this.value = decodeTree;
            var oldVal = this.target;
            this.emitter.$emit("change", { val: this.value, oldVal: oldVal });
            this.emitter.$emit("update", { val: this.value, oldVal: oldVal });
        };
        Observer.prototype._defineReactive = function (target) {
            var _this = this;
            if (!Units.isPrimitive(target)) {
                var children = [];
                if (Array.isArray(target)) {
                    var i = 0;
                    for (var _i = 0, target_1 = target; _i < target_1.length; _i++) {
                        var el = target_1[_i];
                        var child = new Observer("" + i, el);
                        child.on("update", function (response) {
                            _this._childrenDidUpdate();
                        });
                        children.push(child);
                        i++;
                    }
                }
                else {
                    for (var key in target) {
                        var child = new Observer(key, target[key]);
                        child.on("update", function (response) {
                            _this._childrenDidUpdate();
                        });
                        children.push(child);
                    }
                }
                return children;
            }
            else {
                return [];
            }
        };
        Observer.id = 0;
        return Observer;
    }());
    //# sourceMappingURL=Observer.js.map

    var Template = /** @class */ (function () {
        function Template(observer, template) {
            console.log(observer);
            console.log(template);
            for (var prop in template) {
                console.log(prop);
            }
        }
        Template.isSystemProp = function (prop) {
            return Template.SYSTEM_RULES.includes(prop);
        };
        Template.SYSTEM_RULES = ["_$type"];
        return Template;
    }());
    //# sourceMappingURL=Template.js.map

    var Middleware = /** @class */ (function () {
        function Middleware() {
        }
        Middleware.registerMiddleware = function (name, fn) {
            Middleware.globalMiddleware[name] = fn;
        };
        Middleware._useMiddleware = function (value, middleware) {
            if (Units.isArray(middleware) && middleware.length > 0) {
                var result = value;
                // const mid = middleware[0];
                for (var i = 0, l = middleware.length; i < l; i++) {
                    var mid = middleware[i];
                    var nextMid = i === l - 1 ? undefined : middleware[i + 1];
                    if (typeof mid === "function") {
                        result = mid(result);
                    }
                    if (typeof mid === "string") {
                        var globalMid = Middleware.globalMiddleware[mid];
                        if (!!globalMid) {
                            result = globalMid(result);
                        }
                    }
                }
                return result;
            }
            else {
                return value;
            }
        };
        Middleware.globalMiddleware = {};
        return Middleware;
    }());
    //# sourceMappingURL=Middleware.js.map

    var Mozaik = /** @class */ (function () {
        function Mozaik(target, options) {
            this.options = __assign({}, Mozaik.DEFAULT_CONSTRUCTOR_OPTIONS, options);
            this.target$ = target;
            this.tree = [];
            this.getters = {};
            this.tree = [new Observer("root", target)];
        }
        Object.defineProperty(Mozaik.prototype, "target", {
            get: function () {
                return this.tree[0].value;
            },
            set: function (val) {
                this.target$ = val;
            },
            enumerable: true,
            configurable: true
        });
        Mozaik.prototype.get = function (path, defaultValue, options) {
            var $options = __assign({}, Mozaik.DEFAULT_GET_OPTIONS, options);
            if (!!$options.template) {
                $options.getLink = true;
            }
            var middleware = $options.middleware;
            var distructivePath = Units.distructivePath(path);
            distructivePath.unshift("root");
            var result = defaultValue;
            if (distructivePath.length === 1) {
                var observer = this.tree.find(function (el) {
                    return el.name === distructivePath[0];
                });
                if (!!observer) {
                    if ($options.getLink) {
                        result = observer;
                    }
                    else {
                        result = observer.value;
                    }
                }
            }
            else if (distructivePath.length > 1) {
                var ob = this.tree.find(function (el) {
                    return el.name === distructivePath[0];
                });
                distructivePath.shift();
                if (!!ob) {
                    var observer = ob.get(distructivePath);
                    if (!!observer) {
                        if ($options.getLink) {
                            result = observer;
                        }
                        else {
                            result = observer.value;
                        }
                    }
                }
            }
            if (!!$options.template && result instanceof Observer) {
                result = new Template(result, $options.template);
            }
            if (!!middleware) {
                result = Middleware._useMiddleware(result, middleware);
            }
            return result;
        };
        Mozaik.prototype.set = function (path, value, options) {
            var options$ = __assign({}, Mozaik.DEFAULT_SET_OPTIONS, options);
            var parsedPath = Units.distructivePath(path);
            parsedPath.unshift("root");
            var rootOb;
            if (parsedPath.length > 0) {
                var getRootEl = this._getRootObserver(parsedPath[0]);
                if (!!getRootEl) {
                    rootOb = getRootEl;
                }
                else {
                    var deepOb = Units.makeObjectTree(parsedPath.slice(1), value);
                    rootOb = new Observer(parsedPath[0], deepOb);
                    this.tree.push(rootOb);
                }
                parsedPath.shift();
                if (parsedPath.length === 0) {
                    rootOb.value = value;
                }
                else {
                    rootOb.set(parsedPath, value, options$);
                }
            }
        };
        Mozaik.prototype.push = function (path) {
            var items = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                items[_i - 1] = arguments[_i];
            }
            var observer = this.get(path, undefined, { getLink: true });
            if (!!observer) {
                observer.push.apply(observer, items);
            }
        };
        Mozaik.prototype.on = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var path = "";
            var event = "change";
            var cb = function (empty) { return undefined; };
            switch (args.length) {
                case 2:
                    path = args[0];
                    cb = args[1];
                    break;
                case 3:
                    path = args[0];
                    event = args[1];
                    cb = args[2];
                    break;
            }
            var object = path;
            if (typeof object === "string") {
                var observer = this.get(object, undefined, { getLink: true });
                if (!!observer) {
                    return observer.on(event, cb);
                }
            }
        };
        Mozaik.prototype.off = function (path, event) {
            var ob = this.get(path, undefined, { getLink: true });
            if (!!ob) {
                ob.off(event);
            }
        };
        Mozaik.prototype.lock = function (path, options) {
            return undefined;
        };
        Mozaik.prototype.unlock = function (path, options) {
            return undefined;
        };
        Mozaik.prototype.createGetter = function (path) {
            var observer = this.get(path, undefined, {
                getLink: true
            });
            console.log(observer, this.getters, path);
            Object.defineProperty(this.getters, path, {
                get: function () {
                    return observer.value;
                },
                set: function () {
                    return;
                }
            });
        };
        Mozaik.prototype._getRootObserver = function (name) {
            var child = this.tree.find(function (el) {
                return el.name === name;
            });
            return !!child ? child : undefined;
        };
        Mozaik.registerMiddleware = function (name, fn) {
            Middleware.registerMiddleware(name, fn);
        };
        Mozaik.DEFAULT_CONSTRUCTOR_OPTIONS = {
            debug: false
        };
        Mozaik.DEFAULT_GET_OPTIONS = {
            getLink: false
        };
        Mozaik.DEFAULT_READONLY_OPTIONS = {
            deep: false
        };
        Mozaik.DEFAULT_SET_OPTIONS = {
            notify: true
        };
        return Mozaik;
    }());

    //# sourceMappingURL=index.js.map

    return Mozaik;

});
