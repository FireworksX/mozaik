# Mozaikjs
## Install

```sh
npm install mozaikjs --save
```
or 
```sh
yarn add mozaikjs
```

In browser
```html
<script src="node_modules/mozaikjs/dist/mozaik.umd.js"></script>
```

ES Modules
```js
import mozaik from 'mozaik'
```

## Usage

```js
const user = new mozaik({
        user: {
            id: 10,
            name: 'Admin',
            role: 'moderator',
            actions: {
                kick: () => console.log('kick'),
                ban: () => console.log('ban'),
            },
            models: [
                {
                    id: 1,
                    name: 'Art'
                },
                {
                    id: 2,
                    name: 'Blog'
                }
            ]
        }
    });
```

## Methods
### Get - method return value without exeption
```js
// mozaikStore.get('path', defaultValue, options?)

user.get('user.name', 'defaultValue') // Admin
user.get('user.models[0].id', 'defaultValue') // 1
user.get('user.surName', 'defaultValue') // defaultValue
```
### Get params
**getLink** = boolean - return Observer object 
```js
user.get('user.name', 'defaultValue', { getLink: true }) // Observer object
```
**middleware** = (val) => any[]
```js 
user.get('user.name', 'defaultValue', {
        middleware: [
            (val) => val.toUpperCase(), // ADMIN
            (val) => val.slice(0,3), // ADM
        ]
    }) // ADM
```
You can global register middleware
```js
// use mozaik not store
mozaik.registerMiddleware('upper', (val) => val.toUpperCase())

user.get('user.name', 'defaultValue', {
        middleware: [
            'upper', // ADMIN
            (val) => val.slice(0,3), // ADM
        ]
    }) // ADM
```

## Set

```js
// mozaikStore.set('path', newValue, options?)

user.set('user.name', 'Moderator')
user.get('user.name', 'defaultValue') // Moderator
```

## Store Observer

```js
const filter = new mozaik({
    local: {
        id: 15,
        slug: 'New Orlean'
    },
    category: {
        id: 45
    }
})
```

### Subscribe

```js
filter.on('local', ({ newVal, oldVal }) => {
    console.log(`Locale change from ${ oldVal.slug } to ${ val.slug }`)
})

// You can more detail subscribe

filter.on('local.id', ({ val, oldVal }) => {
    console.log(`Locale ID change from ${ oldVal } to ${ val }`)
})

// Trigger for update

filter.set('local.id', 21)
// Locale ID change from 15 to 21

filter.set('local', { id: 37, slug: 'London' })
// Locale ID change from 21 to 37
// Locale change from New Orlean to London

// If you need set value without update hook
filter.set('local.id', 71, { notify: false })

```
**off(path, event)** - unsubscribe for update

```js
const event = filter.on('category.id', ({ val }) => alert(val))
filter.off('category.id', event)
```

## Modules
Mozaik store can be include inside other mozaik store.
Use stores which we already declare

```js

const newCompareStore = new mozaik({
    $modules: {
        userModule: user
        filterModule: filter,
    }
})

// Each module save self subscribers

newCompareStore.set('filterModule.local.id', 130)
// Locale ID change from 71 to 130

newCompareStore.get('userModule.user.id') // 10

```

## Template rules

```js
const newStore = new mozaik({
    name: 'John',
    skills: [
        {
            name: 'JS'
        },
        {
            name: 'TS'
        }
    ]
})

newStore.injectMiddleware('name', (val) => val.toUpperCase())

newStore.get('name') // JOHN

// Also you can use global middleware
mozaik.registerMiddleware('lower', (val) => val.toLowerCase())
newStore.injectMiddleware('name', 'lower')

// Each injectMiddleware collect all middleware

newStore.get('name') // john

// Work with array
newStore.injectMiddleware('skills[0].name', 'lower')

newStore.get('skills[0].name') // js
newStore.get('skills[1].name') // TS

// You can use [*] for use 
newStore.injectMiddleware('skills[*].name', 'lower')

newStore.get('skills[0].name') // js
newStore.get('skills[1].name') // ts

```
