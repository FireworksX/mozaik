import typescript from 'rollup-plugin-typescript';
import uglify from "rollup-plugin-uglify-es";
import commonjs from "rollup-plugin-commonjs";
import cleaner from 'rollup-plugin-cleaner';

const isProduction = !!process.env.production;

console.log(isProduction);
const plugins = [
    typescript(),
    commonjs()
];

if (isProduction) {
    plugins.push(
        cleaner({
            targets: [
                './dist/'
            ]
        }),
        uglify()
    );
}

export default {
    input: './src/index.ts',
    output: [
        {
            file: 'dist/mozaik.cjs.js',
            format: 'cjs',
            name: "mozaik"
        },
        {
            file: 'dist/mozaik.umd.js',
            format: 'umd',
            name: "mozaik"
        },
        {
            file: 'dist/mozaik.amd.js',
            format: 'amd',
            name: "mozaik"
        },
        {
            file: 'dist/mozaik.esm.js',
            format: 'esm',
            name: "mozaik"
        },
    ],
    plugins
}