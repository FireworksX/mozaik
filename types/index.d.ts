
export type Events = "change";
export type FilterRule = "_$type" | string;
import Mozaik from "../src/index";
export default Mozaik;

export interface Istore {
    tree: Iobserver[];
    get(path: string, defaultValue?: any, options?: IstoreGetOptions): any | Iobserver;
    lock(path: string, options?: IlockOptions): void;
    unlock(path: string, options?: IunlockOptions): void;
    set(path: string, value: any, options?: IsetOptions): void;
    push(path: string, ...items: any[]): void;
    on(pathOrObserver: string | Iobserver, event: Events | string, cb: Function): void;
    on(pathOrObserver: string | Iobserver, cb: Function): void;
}

export interface IlockOptions {
    deep?: boolean;
}

export interface IunlockOptions {
    deep?: boolean;
}

export interface IstoreGetOptions {
    getLink?: boolean;
    template?: IfilterRule;
    middleware?: (((val) => any) | string)[];
}

export interface IsetOptions {
    notify?: boolean; // Don`t emit "change". Children don`t update.
}

export interface Ifilter {
    [prop: string]: IfilterRule;
}

export interface IfilterRule {
    _$type?: Function;
    [prop: string]: IfilterRule | any;
}

export interface Iobserver {
    debug?: boolean;
    children: Iobserver[];
    name: string;
    value: any;
    lock(options?: IlockOptions): void;
    unlock(options?: IlockOptions): void;
    getChild(name: string): Iobserver;
    appendChildren(target): Iobserver[];
    push(...items: any[]): void;
    get(path: string[]): Iobserver;
    set(path: string[], value: any, options?: IsetOptions);
    on(event: Events | string, cb: Function): Ievent;
    off(event: Ievent): void;
    off(...event: Ievent[]): void;
}

export interface IgetObserverOptions {
    template?: IfilterRule;
}

export interface Ievent {
    name: string;
    cb: Function;
    readonly _id: number;
}