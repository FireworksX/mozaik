import Event from "./Event";
import { Ievent } from "../../types";

export default class EventEmitter {
    events;

    constructor() {
        this.events = {};
    }

    $on(event: string, cb: Function): Ievent {
        const event$ = new Event(event, cb);
        if (this.events.hasOwnProperty(event)) {
            this.events[event].push(event$);
        } else {
            this.events[event] = [event$];
        }
        return event$;
    }

    $emit(event: string, payload?: any) {
        if (this.events.hasOwnProperty(event)) {
            this.events[event].forEach(el => {
                el.cb(payload);
            });
        }
    }

    $off(event: Ievent) {
        if (this.events.hasOwnProperty(event.name)) {
            const list: Ievent[] = this.events[event.name];
            const index = list
                .map(el => { return el._id; })
                .indexOf(event._id);
            list.splice(index, 1);
        }
    }
}