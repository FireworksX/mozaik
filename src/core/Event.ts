import { Ievent } from "../../types";

export default class Event implements Ievent {

    static _id = 0;
    readonly _id: number;
    name: string;
    cb: Function;

    constructor(name: string, cb: Function) {
        this.name = name;
        this.cb = cb;
        this._id = Event._id;
        Event._id++;
    }


}