import { Units } from "./Units";
import Observer from "./Observer";
import Debug from "../debug";
import Istore, { Ievent, IlockOptions, Iobserver, IsetOptions, IstoreGetOptions, IunlockOptions } from "../../types";
import Template from "./Template";
import Middleware from "./Middleware";

export default class Mozaik implements Istore {
    private static DEFAULT_CONSTRUCTOR_OPTIONS = {
        debug: false
    };
    private static DEFAULT_GET_OPTIONS: IstoreGetOptions = {
        getLink: false
    };
    private static DEFAULT_READONLY_OPTIONS: IlockOptions = {
        deep: false
    };
    private static DEFAULT_SET_OPTIONS: IsetOptions = {
        notify: true
    };

    private target$;
    options;
    private tree;
    public getters;

    constructor(target, options?) {
        this.options = {...Mozaik.DEFAULT_CONSTRUCTOR_OPTIONS, ...options};
        this.target$ = target;
        this.tree = [];
        this.getters = {};

        this.tree = [new Observer("root", target)];
    }

    get target() {
        return this.tree[0].value;
    }

    set target(val) {
        this.target$ = val;
    }

    get(path: string, defaultValue?: any, options?: IstoreGetOptions): any | Iobserver {
        const $options = {...Mozaik.DEFAULT_GET_OPTIONS, ...options};
        if (!!$options.template) {
            $options.getLink = true;
        }
        const middleware = $options.middleware;
        const distructivePath = Units.distructivePath(path);
        distructivePath.unshift("root");
        let result = defaultValue;

        if (distructivePath.length === 1) {
            const observer = this.tree.find(el => {
                return el.name === distructivePath[0];
            });

            if (!!observer) {
                if ($options.getLink) {
                    result = observer;
                } else {
                    result = observer.value;
                }
            }
        } else if (distructivePath.length > 1) {
            const ob = this.tree.find(el => {
                return el.name === distructivePath[0];
            });

            distructivePath.shift();
            if (!!ob) {
                const observer = ob.get(distructivePath);
                if (!!observer) {
                    if ($options.getLink) {
                        result = observer;
                    } else {
                        result = observer.value;
                    }
                }
            }
        }

        if (!!$options.template && result instanceof Observer) {
            result = new Template(result, $options.template);
        }

        if (!!middleware) {
            result = Middleware._useMiddleware(result, middleware);
        }

        return result;
    }

    set(path: string, value: any, options?: IsetOptions) {
        const options$ = {...Mozaik.DEFAULT_SET_OPTIONS, ...options};
        const parsedPath = Units.distructivePath(path);
        parsedPath.unshift("root");
        let rootOb: Iobserver;
        if (parsedPath.length > 0) {
            const getRootEl = this._getRootObserver(parsedPath[0]);
            if (!!getRootEl) {
                rootOb = getRootEl;
            } else {
                const deepOb = Units.makeObjectTree(parsedPath.slice(1), value);
                rootOb = new Observer(parsedPath[0], deepOb);
                this.tree.push(rootOb);
            }
            parsedPath.shift();
            if (parsedPath.length === 0) {
                rootOb.value = value;
            } else {
                rootOb.set(parsedPath, value, options$);
            }
        }
    }

    push(path, ...items: any[]): void {
        const observer = this.get(path, undefined, {getLink: true});
        if (!!observer) {
            observer.push(...items);
        }
    }

    on(...args) {
        let path = "";
        let event = "change";
        let cb = (empty) => { return undefined; };
        switch (args.length) {
            case 2 :
                path = args[0];
                cb = args[1];
                break;
            case 3 :
                path = args[0];
                event = args[1];
                cb = args[2];
                break;
        }
        const object = path;
        if (typeof object === "string") {
            const observer: Iobserver = this.get(object, undefined, {getLink: true});
            if (!!observer) {
                return observer.on(event, cb);
            }
        }
    }

    off(path: string, event: Ievent) {
        const ob = this.get(path, undefined, {getLink: true});
        if (!!ob) {
            ob.off(event);
        }
    }

    lock(path: string, options?: IlockOptions): void {
        return undefined;
    }

    unlock(path: string, options?: IunlockOptions): void {
        return undefined;
    }

    public createGetter(path) {
        const observer = this.get(path, undefined, {
            getLink: true
        });
        console.log(observer, this.getters, path);
        Object.defineProperty(this.getters, path, {
            get(): any {
                return observer.value;
            },
            set() {
                return;
            }
        });
    }

    private _getRootObserver(name: string) {
        const child = this.tree.find(el => {
            return el.name === name;
        });
        return !!child ? child : undefined;
    }

    public static registerMiddleware(name: string, fn: (val) => any) {
        Middleware.registerMiddleware(name, fn);
    }

}
