import { Units } from "./Units";
import Debug from "./../debug/index";
import EventEmitter from "./EventEmitter";
import { Ievent, IlockOptions, Iobserver, IsetOptions, IunlockOptions } from "../../types";

export default class Observer implements Iobserver {
    private target;
    children;
    name: string;
    emitter: EventEmitter;
    static id = 0;
    _id: number;
    debug: boolean;

    constructor(name, target) {
        Observer.id++;
        this.debug = false;
        this._id = Observer.id;
        this.emitter = new EventEmitter();
        this.name = name;
        this.children = [];
        this.target = target;
        this.children = this._defineReactive(this.target);
    }

    get value() {
        return this.target;
    }

    set value(val) {
        const oldVal = this.target;
        // this.emitter.$emit("beforeChange", {val, oldVal});
        if (this.target !== val) {
            if (this.debug) {
                Debug.trace(`Property [${ this.name }] has been changed. Value: ${ oldVal } -> ${ val }.`);
            }
            this.target = val;
            if (!Units.isPrimitive(val)) {
                this.appendChildren(val);
                // this.children = this._defineReactive(val);
            }
            // this.emitter.$emit("change", {val, oldVal});
        }
    }

    lock(options?: IlockOptions) {
        return undefined;
    }

    unlock(options?: IunlockOptions) {
        return undefined;
    }

    get childrenMap() {
        const map = {};
        for (const child of this.children) {
            map[child.name] = child;
        }
        return map;
    }

    getChild(name: string) {
        if (this.childrenMap.hasOwnProperty(name)) {
            return this.childrenMap[name];
        } else {
            return undefined;
        }
    }

    appendChildren(target: any): Iobserver[] {
        let newChildrenMap = {};
        const oldChildren = [];
        if (Units.isObject(target)) {
            for (const key in target) {
                const child = this.getChild(key);
                if (!!child) {
                    child.value = target[key];
                    oldChildren.push(child);
                } else {
                    newChildrenMap[key] = target[key];
                }
            }
        } else if (Units.isArray(target)) {
            newChildrenMap = {...target};
        }
        const childrenTree = this._defineReactive(newChildrenMap);
        // TODO Fix save children when target = object
        this.children = [...this.children, ...childrenTree];
        return this.children;
    }

    push(...items): void {
        if (Units.isArray(this.value)) {
            const lastArrayIndexFromChildren = this.value.length;
            const reactiveItems = this._defineReactive([...items])
                .map((el, index) => {
                    el.name = `${Number(lastArrayIndexFromChildren) + index}`;
                    return el;
                });
            const oldVal = [...this.value];
            this.target.push(...items);
            this.children.push(...reactiveItems);
            this.emitter.$emit("change", {val: this.target, oldVal});
        }
    }

    get(path: string[]): Iobserver {
        if (path.length === 1) {
            return this.getChild(path[0]);
        } else if (path.length > 1) {
            const child = this.getChild(path[0]);
            if (!!child) {
                path.shift();
                return child.get(path);
            }
        }
    }

    set(path: string[], value: any, options: IsetOptions = {}) {
        const { notify } = options;
        if (Units.isArray(path) && path.length > 0) {
            if (path.length === 1) {
                const getChild = this.getChild(path[0]);
                if (!!getChild) {
                    const oldVal = getChild.value;
                    getChild.value = value;
                    if (!!notify) {
                        getChild.emitter.$emit("change", {val: value, oldVal});
                        getChild.emitter.$emit("update", {val: value, oldVal});
                    } else {
                        getChild.emitter.$emit("update", {val: value, oldVal});
                    }
                } else {
                    const ob = new Observer(path[0], value);
                    ob.on("update", (response) => {
                        this._childrenDidUpdate();
                    });
                    this.children.push(ob);
                    const oldVal = ob.value;
                    if (!!notify) {
                        ob.emitter.$emit("change", {val: value, oldVal});
                        ob.emitter.$emit("update", {val: value, oldVal});
                    } else {
                        ob.emitter.$emit("update", {val: value, oldVal});
                    }
                }
            } else if (path.length > 1) {
                const nameOb = path[0];
                path.shift();
                let findOb = this.getChild(nameOb);
                if (!!findOb) {
                    // const oldVal = findOb.value;
                    // findOb.target = value;
                    // this.emitter.$emit("change", {val: value, oldVal});
                } else {
                    const deepTree = Units.makeObjectTree(path, value);
                    findOb = new Observer(nameOb, deepTree);
                    this.children.push(findOb);
                }
                findOb.set(path, value, options);
            }
        }
    }

    on(event: string, cb: Function): Ievent {
        return this.emitter.$on(event, cb);
    }

    off(event: Ievent): void {
        this.emitter.$off(event);
    }

    protected _childrenDidUpdate() {
        const decodeTree = {};
        for (const ob of this.children) {
            decodeTree[ob.name] = ob.value;
        }
        this.value = decodeTree;
        const oldVal = this.target;
        this.emitter.$emit("change", {val: this.value, oldVal});
        this.emitter.$emit("update", {val: this.value, oldVal});
    }

    protected _defineReactive(target): Iobserver[] {
        if (!Units.isPrimitive(target)) {
            const children = [];
            if (Array.isArray(target)) {
                let i = 0;
                for (const el of target) {
                    const child = new Observer(`${i}`, el);
                    child.on("update", (response) => {
                        this._childrenDidUpdate();
                    });
                    children.push(child);
                    i++;
                }
            } else {
                for (const key in target) {
                    const child = new Observer(key, target[key]);
                    child.on("update", (response) => {
                        this._childrenDidUpdate();
                    });
                    children.push(child);
                }
            }
            return children;
        } else {
            return [];
        }
    }

}
