export namespace Units {

    export function isPrimitive(value): boolean {
        return !(value instanceof Object);
    }

    export function distructivePath(path: string): string[] {
        const explodePath = path.split(/[\.\[\]\"\']{1,2}/);
        return explodePath.filter(el => {
            return !!el && el.toString().length > 0;
        });
    }

    export function isObject(target) {
        return target instanceof Object && !Array.isArray(target);
    }

    export function isArray(target) {
        return Array.isArray(target);
    }

    export function isBoolean(target) {
        return typeof target === "boolean";
    }

    export function isNumber(target) {
        return typeof target === "number";
    }

    export function isString(target) {
        return typeof target === "string";
    }

    export function isFunction(target) {
        return typeof target === "function";
    }

    export function isSymbol(target) {
        return typeof target === "symbol";
    }

    export function makeObjectTree(path: string[], value: any, state: any = {}) {
        const state$: any = state;
        if (path.length === 1) {
            return { [path[0]]: value };
        } else if (path.length > 1) {
            const key = path[0];
            path.shift();
            state$[key] = makeObjectTree(path, value);
            return state$;
        }
    }

}