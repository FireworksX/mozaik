import { IfilterRule, Iobserver } from "../../types";

export default class Template {
    private static readonly SYSTEM_RULES = ["_$type"];

    constructor(observer: Iobserver, template: IfilterRule) {
        console.log(observer);
        console.log(template);
        for (const prop in template) {
            console.log(prop);
        }
    }

    private static isSystemProp(prop): boolean {
        return Template.SYSTEM_RULES.includes(prop);
    }

}