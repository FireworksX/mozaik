import { Units } from "./Units";

export default class Middleware {
    private static globalMiddleware: any = {};

    public static registerMiddleware(name: string, fn: (val) => any) {
        Middleware.globalMiddleware[name] = fn;
    }

    public static _useMiddleware(value, middleware): any {
        if (Units.isArray(middleware) && middleware.length > 0) {
            let result = value;
            // const mid = middleware[0];
            for (let i = 0, l = middleware.length; i < l; i++) {
                const mid = middleware[i];
                const nextMid = i === l - 1 ? undefined : middleware[i + 1];
                if (typeof mid === "function") {
                    result = mid(result);
                }
                if (typeof mid === "string") {
                    const globalMid = Middleware.globalMiddleware[mid];
                    if (!!globalMid) {
                        result = globalMid(result);
                    }
                }
            }
            return result;
        } else {
            return value;
        }
    }

}
