import { Iobserver } from "../../types";

namespace Debug {

    export function trace(message: string) {
        console.trace(`[Mozaik] ${ message }`);
    }

    export function warn(message: string) {
        console.warn(`[Mozaik] ${ message }`);
    }

    export function info(message: string, ...options) {
        console.info(`[Mozaik] ${ message }`, ...options);
    }

    export function mutation(ob: Iobserver, newVal, oldVal) {
        this.info("mutation");
    }

}

export default Debug;

